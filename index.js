/*
This is a Telegram bot that suggests recipes to you and your friends.
*/


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Example for API return -> Use this for debugging
var data = {
"recipes": [{
    "vegetarian": true,
    "vegan": false,
    "glutenFree": false,
    "dairyFree": true,
    "veryHealthy": false,
    "cheap": false,
    "veryPopular": false,
    "sustainable": false,
    "weightWatcherSmartPoints": 15,
    "gaps": "no",
    "lowFodmap": false,
    "sourceUrl": "http://www.foodista.com/recipe/DXMVKPRX/dessert-apple-rings-with-cinnamon-cream-syrup",
    "spoonacularSourceUrl": "https://spoonacular.com/dessert-apple-rings-with-cinnamon-cream-syrup-641435",
    "aggregateLikes": 49,
    "spoonacularScore": 28.0,
    "healthScore": 2.0,
    "creditsText": "Foodista.com – The Cooking Encyclopedia Everyone Can Edit",
    "license": "CC BY 3.0",
    "sourceName": "Foodista",
    "pricePerServing": 150.42,
    "extendedIngredients": [{
        "id": 20081,
        "aisle": "Baking",
        "image": "flour.png",
        "consitency": "solid",
        "name": "all purpose flour",
        "original": "1 cup all purpose flour",
        "originalString": "1 cup all purpose flour",
        "originalName": "all purpose flour",
        "amount": 1.0,
        "unit": "cup",
        "meta": [],
        "metaInformation": [],
        "measures": {
            "us": {
                "amount": 1.0,
                "unitShort": "cup",
                "unitLong": "cup"
            },
            "metric": {
                "amount": 236.588,
                "unitShort": "ml",
                "unitLong": "milliliters"
            }
        }
    }, {
        "id": 4582,
        "aisle": "Oil, Vinegar, Salad Dressing",
        "image": "vegetable-oil.jpg",
        "consitency": "liquid",
        "name": "canola oil",
        "original": "Canola oil for frying",
        "originalString": "Canola oil for frying",
        "originalName": "Canola oil for frying",
        "amount": 6.0,
        "unit": "servings",
        "meta": ["for frying"],
        "metaInformation": ["for frying"],
        "measures": {
            "us": {
                "amount": 6.0,
                "unitShort": "servings",
                "unitLong": "servings"
            },
            "metric": {
                "amount": 6.0,
                "unitShort": "servings",
                "unitLong": "servings"
            }
        }
    }, {
        "id": 2010,
        "aisle": "Spices and Seasonings",
        "image": "cinnamon.jpg",
        "consitency": "solid",
        "name": "cinnamon",
        "original": "1/2 tsp cinnamon",
        "originalString": "1/2 tsp cinnamon",
        "originalName": "cinnamon",
        "amount": 0.5,
        "unit": "tsp",
        "meta": [],
        "metaInformation": [],
        "measures": {
            "us": {
                "amount": 0.5,
                "unitShort": "tsps",
                "unitLong": "teaspoons"
            },
            "metric": {
                "amount": 0.5,
                "unitShort": "tsps",
                "unitLong": "teaspoons"
            }
        }
    }, {
        "id": 19350,
        "aisle": "Baking",
        "image": "corn-syrup.png",
        "consitency": "solid",
        "name": "corn syrup",
        "original": "1/2 cup corn syrup",
        "originalString": "1/2 cup corn syrup",
        "originalName": "corn syrup",
        "amount": 0.5,
        "unit": "cup",
        "meta": [],
        "metaInformation": [],
        "measures": {
            "us": {
                "amount": 0.5,
                "unitShort": "cups",
                "unitLong": "cups"
            },
            "metric": {
                "amount": 118.294,
                "unitShort": "ml",
                "unitLong": "milliliters"
            }
        }
    }, {
        "id": 1124,
        "aisle": "Milk, Eggs, Other Dairy",
        "image": "egg-white.jpg",
        "consitency": "solid",
        "name": "egg white",
        "original": "1 large egg white",
        "originalString": "1 large egg white",
        "originalName": "egg white",
        "amount": 1.0,
        "unit": "large",
        "meta": [],
        "metaInformation": [],
        "measures": {
            "us": {
                "amount": 1.0,
                "unitShort": "large",
                "unitLong": "large"
            },
            "metric": {
                "amount": 1.0,
                "unitShort": "large",
                "unitLong": "large"
            }
        }
    }, {
        "id": 1089003,
        "aisle": "Produce",
        "image": "grannysmith-apple.png",
        "consitency": "solid",
        "name": "granny smith apples",
        "original": "6 large Granny Smith apples, peeled",
        "originalString": "6 large Granny Smith apples, peeled",
        "originalName": "Granny Smith apples, peeled",
        "amount": 6.0,
        "unit": "large",
        "meta": ["peeled"],
        "metaInformation": ["peeled"],
        "measures": {
            "us": {
                "amount": 6.0,
                "unitShort": "large",
                "unitLong": "larges"
            },
            "metric": {
                "amount": 6.0,
                "unitShort": "large",
                "unitLong": "larges"
            }
        }
    }, {
        "id": 2047,
        "aisle": "Spices and Seasonings",
        "image": "salt.jpg",
        "consitency": "solid",
        "name": "salt",
        "original": "1/4 teaspoon salt",
        "originalString": "1/4 teaspoon salt",
        "originalName": "salt",
        "amount": 0.25,
        "unit": "teaspoon",
        "meta": [],
        "metaInformation": [],
        "measures": {
            "us": {
                "amount": 0.25,
                "unitShort": "tsps",
                "unitLong": "teaspoons"
            },
            "metric": {
                "amount": 0.25,
                "unitShort": "tsps",
                "unitLong": "teaspoons"
            }
        }
    }, {
        "id": 19335,
        "aisle": "Baking",
        "image": "sugar-in-bowl.png",
        "consitency": "solid",
        "name": "sugar",
        "original": "1 cup sugar",
        "originalString": "1 cup sugar",
        "originalName": "sugar",
        "amount": 1.0,
        "unit": "cup",
        "meta": [],
        "metaInformation": [],
        "measures": {
            "us": {
                "amount": 1.0,
                "unitShort": "cup",
                "unitLong": "cup"
            },
            "metric": {
                "amount": 236.588,
                "unitShort": "ml",
                "unitLong": "milliliters"
            }
        }
    }, {
        "id": 2050,
        "aisle": "Baking",
        "image": "vanilla.jpg",
        "consitency": "solid",
        "name": "vanilla",
        "original": "1 tablespoon vanilla",
        "originalString": "1 tablespoon vanilla",
        "originalName": "vanilla",
        "amount": 1.0,
        "unit": "tablespoon",
        "meta": [],
        "metaInformation": [],
        "measures": {
            "us": {
                "amount": 1.0,
                "unitShort": "Tbsp",
                "unitLong": "Tbsp"
            },
            "metric": {
                "amount": 1.0,
                "unitShort": "Tbsp",
                "unitLong": "Tbsp"
            }
        }
    }, {
        "id": 14412,
        "aisle": "Beverages",
        "image": "water.png",
        "consitency": "liquid",
        "name": "water",
        "original": "1 cup water",
        "originalString": "1 cup water",
        "originalName": "water",
        "amount": 1.0,
        "unit": "cup",
        "meta": [],
        "metaInformation": [],
        "measures": {
            "us": {
                "amount": 1.0,
                "unitShort": "cup",
                "unitLong": "cup"
            },
            "metric": {
                "amount": 236.588,
                "unitShort": "ml",
                "unitLong": "milliliters"
            }
        }
    }],
    "id": 641435,
    "title": "Dessert Apple Rings With Cinnamon Cream Syrup",
    "readyInMinutes": 45,
    "servings": 6,
    "image": "https://spoonacular.com/recipeImages/641435-556x370.jpg",
    "imageType": "jpg",
    "summary": "Dessert Apple Rings With Cinnamon Cream Syrup might be just the dessert you are searching for. This recipe makes 6 servings with <b>402 calories</b>, <b>3g of protein</b>, and <b>2g of fat</b> each. For <b>$1.32 per serving</b>, this recipe <b>covers 6%</b> of your daily requirements of vitamins and minerals. This recipe from Foodista has 49 fans. A mixture of all purpose flour, canolan oil, egg white, and a handful of other ingredients are all it takes to make this recipe so tasty. It is a good option if you're following a <b>dairy free and vegetarian</b> diet. From preparation to the plate, this recipe takes about <b>45 minutes</b>. All things considered, we decided this recipe <b>deserves a spoonacular score of 29%</b>. This score is rather bad. Try <a href=\"https://spoonacular.com/recipes/whole-wheat-apple-cinnamon-pancakes-with-cinnamon-syrup-548079\">Whole Wheat Apple Cinnamon Pancakes with Cinnamon Syrup</a>, <a href=\"https://spoonacular.com/recipes/cinnamon-cornbread-waffles-with-apple-cinnamon-syrup-165951\">Cinnamon-Cornbread Waffles with Apple-Cinnamon Syrup</a>, and <a href=\"https://spoonacular.com/recipes/apple-cinnamon-waffles-with-cinnamon-syrup-568719\">Apple Cinnamon Waffles with Cinnamon Syrup</a> for similar recipes.",
    "cuisines": [],
    "dishTypes": ["dessert"],
    "diets": ["dairy free", "lacto ovo vegetarian"],
    "occasions": [],
    "winePairing": {},
    "instructions": "<ol><li>Whisk egg until frothy and the whisk in water and vanilla. Whisk in flour and salt and let sit while slicing apples.</li><li>Slice peeled apples into 1/2 inch thick slices (you will get about 4 slices per apple).</li><li>Using varying sized biscuit cutters, cut rings out of apple slices, discarding smallest circle containing core.</li><li>Heat oil to 375 degrees.</li><li>Dip apple rings into batter, letting excess drip off.</li><li>Cook in oil in small batches, turning frequently to monitor browning.</li><li>When the rings turn golden brown, remove to a plate and while still hot sprinkle generously with cinnamon sugar.</li><li>Serve immediately with warm drizzled sauce.</li><li>Makes 16 apple rings  serves 4 to 6.</li><li>CINNAMON CREAM SYRUP</li><li>In a small saucepan, combine 1 cup sugar, 1/2 cup corn syrup and 1/2 teaspoon cinnamon. Bring to a boil, stirring for 3 minutes. Remove from heat and cool for 5 minutes. Stir in 1/2 cup evaporated milk. Serve warm.</li></ol>",
    "analyzedInstructions": [{
        "name": "",
        "steps": [{
            "number": 1,
            "step": "Whisk egg until frothy and the whisk in water and vanilla.",
            "ingredients": [{
                "id": 2050,
                "name": "vanilla",
                "image": "vanilla.jpg"
            }, {
                "id": 14412,
                "name": "water",
                "image": "water.png"
            }],
            "equipment": [{
                "id": 404661,
                "name": "whisk",
                "image": "whisk.png"
            }]
        }, {
            "number": 2,
            "step": "Whisk in flour and salt and let sit while slicing apples.Slice peeled apples into 1/2 inch thick slices (you will get about 4 slices per apple).Using varying sized biscuit cutters, cut rings out of apple slices, discarding smallest circle containing core.",
            "ingredients": [{
                "id": 9003,
                "name": "apple",
                "image": "apple.jpg"
            }, {
                "id": 20081,
                "name": "all purpose flour",
                "image": "flour.png"
            }, {
                "id": 2047,
                "name": "salt",
                "image": "salt.jpg"
            }],
            "equipment": [{
                "id": 404661,
                "name": "whisk",
                "image": "whisk.png"
            }]
        }, {
            "number": 3,
            "step": "Heat oil to 375 degrees.Dip apple rings into batter, letting excess drip off.Cook in oil in small batches, turning frequently to monitor browning.When the rings turn golden brown, remove to a plate and while still hot sprinkle generously with cinnamon sugar.",
            "ingredients": [{
                "id": 10219335,
                "name": "cinnamon sugar",
                "image": "cinnamon-sugar.png"
            }, {
                "id": 9003,
                "name": "apple",
                "image": "apple.jpg"
            }, {
                "id": 4582,
                "name": "cooking oil",
                "image": "vegetable-oil.jpg"
            }],
            "equipment": []
        }, {
            "number": 4,
            "step": "Serve immediately with warm drizzled sauce.Makes 16 apple rings  serves 4 to 6.CINNAMON CREAM SYRUPIn a small saucepan, combine 1 cup sugar, 1/2 cup corn syrup and 1/2 teaspoon cinnamon. Bring to a boil, stirring for 3 minutes.",
            "ingredients": [{
                "id": 19350,
                "name": "corn syrup",
                "image": "corn-syrup.png"
            }, {
                "id": 2010,
                "name": "cinnamon",
                "image": "cinnamon.jpg"
            }, {
                "id": 9003,
                "name": "apple",
                "image": "apple.jpg"
            }, {
                "id": 19335,
                "name": "sugar",
                "image": "sugar-in-bowl.png"
            }],
            "equipment": [{
                "id": 404669,
                "name": "sauce pan",
                "image": "sauce-pan.jpg"
            }],
            "length": {
                "number": 3,
                "unit": "minutes"
            }
        }, {
            "number": 5,
            "step": "Remove from heat and cool for 5 minutes. Stir in 1/2 cup evaporated milk.",
            "ingredients": [],
            "equipment": [],
            "length": {
                "number": 5,
                "unit": "minutes"
            }
        }, {
            "number": 6,
            "step": "Serve warm.",
            "ingredients": [],
            "equipment": []
        }]
    }]
}]
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
PREP
*/

process.env["NTBA_FIX_319"] = 1

const fs = require('fs');
const https = require('https');

// Load keys
var keys = require('./data/keys.json')

// Create Telegram bot
const TelegramBot = require('node-telegram-bot-api')
const token = keys.bot
const bot = new TelegramBot(token, {polling: true})

const spoonacularApiKey = keys.spoonacular

// Data base for own recipes
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('data/db.json')
const db = low(adapter)


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Help and instruction text
// TODO: Beispielbefehle anpassen und ergänzen
bot.onText(/\/(hilfe|help|Hilfe|Help|start|Start)/, (msg, match) => {
  var chatId = msg.chat.id;
  let response = "<b>Hilfe - Kochbot</b>\n\n\n" +
                 "Hallo,\nich bin ein Bot, der dir Rezepte vorschlägt, wenn du dich mal wieder nicht entscheiden kannst, was es heute Leckeres geben soll. Außerdem helfe ich dir deine eigene Rezeptesammlung zu verwalten." +
                 "\n\n" +
                 "<b>Was kann ich?</b>\n" +
                 "Ich enpfehle dir großartige Rezepte, angepasst auf deine Ernährungsform (z.B. vegetarisch, vegan). Dabei kann ich dir je nach Wunsch etwas Experimentelles oder Altbekanntes aus deiner persönlichen Rezeptesammlung vorschlagen." +
                 "\n\n" +
                 "<b>Wie kommuniziert man mit mir?</b>\n" +
                 "Für jedes Problem gibt es einen Befehl, den du mir schicken kannst. " +
                 "Befehle findest du über die Auto-Vervollständigung von Telegram. Du musst bloß eine Nachricht tippen, die mit / beginnt. " +
                 "Alternativ gibt es in den meisten Telegram-Versionen rechts neben dem Textfeld einen quadratischen Button mit einem /. " +
                 "Hierüber kannst du aus einigen verfügbaren Befehlen auswählen." +
                 "\n\n" +
                 "<b>Mögliche Befehle sind z.B.:</b>" +
                 "\n" +
                 "<code>- /neu</code>\n" +
                 "Über diesen Befehl kannst du eigene Rezepte in deiner Rezeptesammlung anlegen.\n" +
                 "<code>- /experimentell</code>\n" +
                 "Schlägt dir ein dir noch unbekanntes Rezept vor.\n" +
                 "<code>- /bekannt</code>\n" +
                 "Schlägt dir ein Rezept aus deiner individuellen Rezeptesammlung vor.\n" +
                 "<code>- /random</code>\n" +
                 "Schlägt dir mit 70%-iger Wahrscheinlichkeit ein Rezept aus deiner Rezeptesammlung vor. Mit 30%-iger Wahrscheinlichkeit wird dir etwas Unbekanntes empfohlen.\n" +
                 "<code>- /random vegan Nachspeise</code>\n" +
                 "Jeden Befehl zum Rezeptevorschlag kannst du mit verschiedenen Optionen aufrufen, die du wiederum beliebig miteinander kombinieren kannst.\n" +
                 "<i>"+
                 "Optionen:\n" +
                 "- vegan\n" +
                 "- vegetarisch\n" +
                 "- Vorspeise\n" +
                 "- Hauptspeise\n" +
                 "- Nachspeise\n" +
                 "- Frühstück\n" +
                 "- Snack\n" +
                 "</i>" +
                 "\n" +
                 "Guten Appetit! 😋" +
                 "\n\n\n" +
                 "Du hast Fragen? Verbesserungsvorschläge? Ideen? Dann sprich einfach <a href='https://t.me/ClarissaStaudt'>@ClarissaStaudt</a> oder <a href='https://t.me/Tobidju'>@Tobidju</a> an."
  bot.sendMessage(chatId, response, { parse_mode: "HTML" })
});

////////////////////////////////////////////////

// Create a new recipe

bot.onText(/\/(neu)/, (msg, match) => {
    var chatId = msg.chat.id

    // Create empty recipe
    let loc = chatId + '.neu'
    let emptyRecipe = {
      "vegetarian": false,
      "vegan": false,
      "creditsText": "?",
      "extendedIngredients": [],
      "title": "",
      "readyInMinutes": 0,
      "servings": 0,
      "image": "",
      "dishTypes": [],
      "analyzedInstructions": []
    }
    db.set(loc, emptyRecipe).write()

    let response =  "<b>Neues Rezept anlegen</b>\n\n" +
                    "Hallo, im Folgenden erstelle ich gemeinsam mit dir ein Rezept.\n\n" +
                    "Falls bei der Eingabe Fehler auftreten, kannst du diese korrigieren, indem du den gleichen Befehl mit den korrekten Optionen nochmal an mich schickst. " +
                    "Deine vorherige Angabe wird dadurch automatisch überschrieben."
    bot.sendMessage(chatId, response, { parse_mode: "HTML" }).then(() => {
        response = "Bitte gib den Titel deines Rezeptes an. " +
                    "Nutze folgendes Format:\n" +
                    "/titel Titel\n\n" +
                    "<code>/titel Einfacher Schokokeks</code>"
        bot.sendMessage(chatId, response, { parse_mode: "HTML" })
    })

})

bot.onText(/\/titel (.+)/, (msg, match) => {
    var chatId = msg.chat.id

    let loc = chatId + ".neu.title"
    db.set(loc, match[1]).write()

    let response = "Danke. Von wem ist das Rezept? " +
                    "Nutze folgendes Format:\n" +
                    "/autor Autor\n\n" +
                    "<code>/autor Oma Greta</code>"
    bot.sendMessage(chatId, response, { parse_mode: "HTML" })
})

bot.onText(/\/autor (.+)/, (msg, match) => {
    var chatId = msg.chat.id

    let loc = chatId + ".neu.creditsText"
    db.set(loc, match[1]).write()

    let response = "Danke. Welche Zutaten benötigt man? " +
                    "Nutze folgendes Format:\n" +
                    "/zutaten\nMenge Einheit Zutat\n\n" +
                    "<code>/zutaten\n250 g Schokolade (Vollmilch)\n40 Stück Butterkekse</code>"
    bot.sendMessage(chatId, response, { parse_mode: "HTML" })
})

bot.onText(/\/zutaten\n(.+)/, (msg, match) => {
    var chatId = msg.chat.id

    let loc = chatId + ".neu.extendedIngredients"

    match = msg.text
    match = match.substr(match.indexOf('/zutaten') + 9)
    match = match.split("\n")

    match.forEach(i => {
        i = i.split(" ")
        let el = {
            "name": i.slice(2).join(" "),
            "measures": {
                "metric": {
                    "amount": i[0],
                    "unitShort": i[1]
                }
            }
        }
        db.get(loc).push(el).write()
    })

    let response = "Danke. Wie wird das Rezept zubereitet? " +
                    "Nutze folgendes Format:\n" +
                    "/zubereitung\nSchrittnummer Beschreibung\n\n" +
                    "<code>/zubereitung\n" +
                    "1 Erhitze die Schokolade im Wasserbad.\n" +
                    "2 Tauche die Kekse in die geschmolzene Schokolade.</code>"
    bot.sendMessage(chatId, response, { parse_mode: "HTML" })
})

bot.onText(/\/zubereitung\n(.+)/, (msg, match) => {
    var chatId = msg.chat.id

    let loc = chatId + ".neu.analyzedInstructions"
    db.set(loc, [{"steps": []}]).write()
    loc = loc + "[0].steps"

    match = msg.text
    match = match.substr(match.indexOf('/zubereitung') + 13)
    match = match.split("\n")

    match.forEach(s => {
        let number = s.substr(0,s.indexOf(' '))
        let step = s.substr(s.indexOf(' ') + 1)
        let el = {
            "number": number,
            "step": step
        }
        db.get(loc).push(el).write()
    })

    let response = "Danke. Wie lange dauert die Zubereitung in Minuten? " +
                    "Nutze folgendes Format:\n" +
                    "/zubereitungsdauer Minutenangabe\n\n" +
                    "<code>/zubereitungsdauer " +
                    "10</code>"
    bot.sendMessage(chatId, response, { parse_mode: "HTML" })
})

bot.onText(/\/zubereitungsdauer (.+)/, (msg, match) => {
    var chatId = msg.chat.id

    let loc = chatId + ".neu.readyInMinutes"
    db.set(loc, match[1]).write()

    let response = "Danke. Wie viele Portionen ergibt das Rezept? " +
                    "Nutze folgendes Format:\n" +
                    "/portionen Portionenanzahl\n\n" +
                    "<code>/portionen " +
                    "5</code>"
    bot.sendMessage(chatId, response, { parse_mode: "HTML" })
})

bot.onText(/\/portionen (.+)/, (msg, match) => {
    var chatId = msg.chat.id

    let loc = chatId + ".neu.servings"
    db.set(loc, match[1]).write()

    let response = "Danke. Ist das Rezept vegetarisch? " +
                    "Nutze folgendes Format:\n" +
                    "/vegetarisch ja/nein\n\n" +
                    "<code>/vegetarisch " +
                    "ja</code>"
    bot.sendMessage(chatId, response, { parse_mode: "HTML" })
})

bot.onText(/\/vegetarisch (.+)/, (msg, match) => {
    var chatId = msg.chat.id

    let loc = chatId + ".neu.vegetarian"
    let veg = ""
    if (match[1] == "ja") {
        veg = true
    }
    else if (match[1] == "nein") {
        veg = false
    }
    db.set(loc, veg).write()

    let response = "Danke. Ist das Rezept vegan? " +
                    "Nutze folgendes Format:\n" +
                    "/vegan ja/nein\n\n" +
                    "<code>/vegan " +
                    "nein</code>"
    bot.sendMessage(chatId, response, { parse_mode: "HTML" })
})

bot.onText(/\/vegan (.+)/, (msg, match) => {
    var chatId = msg.chat.id

    let loc = chatId + ".neu.vegan"
    let veg = ""
    if (match[1] == "ja") {
        veg = true
    }
    else if (match[1] == "nein") {
        veg = false
    }
    db.set(loc, veg).write()

    let response = "Danke. Unter welche der folgenden Kategorien fällt dein Rezept?\n" +
                    "<i>Vorspeise Salat Suppe Hauptspeise Nachspeise Frühstück Brot Snack Fingerfood Beilage</i>\n" +
                    "Nutze folgendes Format:\n" +
                    "/essenstyp Kategorie1 Kategorie2\n\n" +
                    "<code>/essenstyp Nachspeise Snack</code>"
    bot.sendMessage(chatId, response, { parse_mode: "HTML" })
})

bot.onText(/\/essenstyp (.+)/, (msg, match) => {
    var chatId = msg.chat.id

    let dishTypes = getOptions(match[1])

    let loc = chatId + ".neu.dishTypes"
    db.set(loc, dishTypes).write()

    let recipe = db.get(chatId + ".neu").value()

    let response = "Danke. Hier ist dein fertiges Rezept:\n\n"
    bot.sendMessage(chatId, response, { parse_mode: "HTML" })

    response = generateRecipeResponseDB(recipe)
    bot.sendMessage(chatId, response, { parse_mode: "HTML" })

    response = "Wenn du das Rezept so speichern möchtest, schreibe\n" +
                "<code>/speichern</code>"

    // TODO also send back the classification info (vegan, vegetarian, dishTypes)

})

bot.onText(/\/speichern/, (msg, match) => {
    var chatId = msg.chat.id

    //  Include finished recipe in recipes
    loc = chatId + ".recipes"
    if (!db.has(loc).value()){
        db.set(loc, []).write()
    }
    db.get(loc).push(recipe).write()

    // Remove finished recipe from neu
    loc = chatId + ".neu"
    db.set(loc, {}).write()

    let response = "Dein Rezept wurde erfolgreich gespeichert."
    bot.sendMessage(chatId, response, { parse_mode: "HTML" })
})

////////////////////////////////////////////////

bot.onText(/\/(experimentell|bekannt|random)?(.+)/, (msg, match) => {
  var chatId = msg.chat.id
  let options = []

  // Experimentell
  if ((match[1] === undefined && match[2] == "experimentell") || (match[1] == "experimentell")) {
      if (match[1] == "experimentell") {
         options = getOptions(match[2]).join(",")
      }
      requestFromApi(options, chatId)
  }

  // bekannt
  if ((match[1] === undefined && match[2] == "bekannt") || (match[1] == "bekannt")) {
      if (match[1] == "bekannt") {
         options = getOptions(match[2])
      }

      // If a user does not have recipes, we return an error
      if(db.has(chatId).value()) {
          requestFromDb(chatId, options)
      }
      else {
          // TODO: improve error message
          let response = "<b>Fehler</b>\n" +
                        "Leider scheinst du noch keine eigenen Rezepte gespeichert zu haben."
          bot.sendMessage(chatId, response, { parse_mode: "HTML" })
      }

  }

  // random (70% eigene Rezepte, 30% Neues)
  if ((match[1] === undefined && match[2] == "random") || (match[1] == "random")) {
      if (match[1] == "random") {
         options = getOptions(match[2])
      }

      // If a user does not have recipes, we always have to generate an API request
      let ownsRecipes = false
      if (db.has(chatId).value()) {
          ownsRecipes = true
      }

      let randomNr = Math.floor(Math.random() * Math.floor(10)) // random number between 0 and 9

      if (randomNr < 2 || !ownsRecipes) {
          requestFromApi(options, chatId)
      }
      else {
          requestFromDb(chatId, options)
      }
  }

});


////////////////////////////////////////////////

// Returns a list of options generated from user input (e.g. vegan)
function getOptions(match) {
    // Split into options
    match = match.split(" ")

    // Translate options
    let options = []
    if (match.includes("vegan")) {options.push("vegan")}
    if (match.includes("vegetarisch")) {options.push("vegetarian")}
    if (match.includes("Vorspeise")) {
        options.push("appetizer")
        options.push("salad")
        options.push("soup")
    }
    if (match.includes("Hauptspeise")) {options.push("main-course")}
    if (match.includes("Nachspeise")) {options.push("dessert")}
    if (match.includes("Frühstück")) {
        options.push("breakfast")
        options.push("bread")
    }
    if (match.includes("Snack")) {
        options.push("snack")
        options.push("fingerfood")
        options.push("side-dish")
    }

    return(options)
}

////////////////////////////////////////////////


function requestFromApi(options, chatId) {
    let requestURL = "https://api.spoonacular.com/recipes/random?apiKey=" + spoonacularApiKey + "&number=1&tags=" + options + "&limitLicense=true"
    console.log(requestURL)

    // Auskommentiert, wegen API Restriktionen
    /*https.get(requestURL, (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
          data += chunk
        });
        resp.on('end', () => {
        */
            //data = JSON.parse(data) // TODO: zusammen mit http Request wieder einkommentieren
            data = data.recipes[0]
            let response = generateRecipeResponseAPI(data)
            bot.sendMessage(chatId, response, { parse_mode: "HTML" }).then(() => {})
      /*
        });

      }).on("error", (err) => {
        console.log("Error: " + err.message)
      });
      */
}


function requestFromDb(chatId, options) {

    let loc = chatId + ".recipes"
    let userRecipes = db.get(loc).value() // get this users recipes

    // Filter according to options
    if (options.includes("vegan")) {
        userRecipes = userRecipes.filter(r => (r.vegan == true))
    }
    if (options.includes("vegetarian")) {
        userRecipes = userRecipes.filter(r => (r.vegetarian == true))
    }

    let mealtype = ["appetizer", "salad", "soup", "main-course", "dessert", "breakfast", "bread", "snack", "fingerfood", "side-dish"]
    let recipelist = []
    let appliedFilter = false
    mealtype.forEach((t) => {
        if (options.includes(t)) {
            let filtered =  userRecipes.filter(r => (r.dishTypes.includes(t)))
            recipelist.push(filtered)
            appliedFilter = true
        }
    })
    if (appliedFilter) {
        userRecipes = [].concat.apply([], recipelist)
    }

    // Draw a random recipe
    let randomNr = Math.floor(Math.random() * Math.floor(userRecipes.length))
    let recipe = userRecipes[randomNr]

    // Generate response
    let response = generateRecipeResponseDB(recipe)
    bot.sendMessage(chatId, response, { parse_mode: "HTML" })
}

////////////////////////////////////////////////

// Formats instructions and returns them as string
function getInstructions(data) {
    let instructions = ""
    data.analyzedInstructions[0].steps.forEach((i) => {
        let step = i.step
        step = step.split(".").join(". ")
        instructions += "<b>" + i.number + "</b>. " + step + "\n"
    })
    return(instructions)
}

// Formats ingredients and returns them as string
function getIngredients(data) {
    let ingredients = ""
    data.extendedIngredients.forEach((i) => {
        ingredients += "- " + i.measures.metric.amount + " " + i.measures.metric.unitShort + " " + i.name + "\n"
    })
    return(ingredients)
}

// Generates a response string for a recipe
function generateRecipeResponseAPI(data) {

    let ingredients = getIngredients(data)
    let instructions = getInstructions(data)

    let response =
        "<b>" + data.title + "</b>" + "\n\n" +
        "<b>🛒Ingredients:</b>\n" + ingredients + "\n" +
        "<b>👩🏻‍🍳 Instructions:</b>\n" + instructions + "\n" +
        "⏰ Ready in " + data.readyInMinutes + " minutes\n" +
        "🍽 " + data.servings + " servings\n\n" +
         "<i>" +
        "From <a href='" + data.sourceUrl + "'>" + data.creditsText + ".</a>\n" +
        "Powered by <a href='" + data.spoonacularSourceUrl + "'>spoonacular.</a>" +
        "</i>"
    return(response)
}

// Generates a response string for a recipe
function generateRecipeResponseDB(data) {

    let ingredients = getIngredients(data)
    let instructions = getInstructions(data)

    let response =
        "<b>" + data.title + "</b>" + "\n\n" +
        "<b>🛒 Zutaten:</b>\n" + ingredients + "\n" +
        "<b>👩🏻‍🍳 Zubereitung:</b>\n" + instructions + "\n" +
        "⏰ Zubereitungsdauer " + data.readyInMinutes + " Minuten\n" +
        "🍽 " + data.servings + " Portionen\n\n" +
         "<i>" +
         "Rezept von " +
         data.creditsText
         + "</i>"

    return(response)
}
